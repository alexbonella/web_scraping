import pandas as pd 
import requests
from bs4 import BeautifulSoup
import json
import credentials
import numpy as np
import argparse
import random
import time
from datetime import datetime
import datetime
from tqdm import tqdm


# Get permission
DEVELOPER_KEY = credentials.DEVELOPER_KEY
CHANNEL_ID = credentials.channel_id
os.chdir('/home/ubuntu/de_latam_scripts')

# Aleatory component
tiempo = [1,2,4,6,8.5,2.3,3.5,4.4,5.3,6.7,3.2]


info_page = []
pages_number = 10
results_by_page = 20 
url_initial='https://www.googleapis.com/youtube/v3/search?key='+DEVELOPER_KEY+'&channelId='+CHANNEL_ID+'&part=id&part=snippet&order=date'


# Info videos 
for i in tqdm(range(pages_number)):
    
    
    response_1 = requests.get(url_initial)
    
    try :
        
        info_page.append(json.loads(response_1.content))
        response_temp = json.loads(response_1.content)
        pageToken = response_temp['nextPageToken']
        url_initial = 'https://www.googleapis.com/youtube/v3/search?key='+DEVELOPER_KEY+'&channelId='+CHANNEL_ID+'&part=id&part=snippet&maxResults='+str(results_by_page)+'&pageToken='+pageToken
    except :
        print('Aqui vamos ' + str(i))
        print(response_1)
        
    time.sleep(random.choices(tiempo)[0])

# Build Dataframe
batch_etag = []
prev_pag_token = []
next_pag_token = []
region = []
resources_type = [] 
id_item = []
url_video = []
channel = []
channel_id = []
video_title = []
video_description = []
created_at = []

for i in tqdm(range(len(info_page))):
    
    try :
        
        batch_etag_ini = info_page[i]['etag']
        prev_pag_token_ini = info_page[i]['prevPageToken']
        next_pag_token_ini= info_page[i]['nextPageToken']
        region_ini = info_page[i]['regionCode']
        element = 0

        for j in range(len(info_page[i]['items'])):

            element = list(info_page[i]['items'][j]['id'].keys())

            batch_etag.append(batch_etag_ini)
            prev_pag_token.append(prev_pag_token_ini)
            next_pag_token.append(next_pag_token_ini)
            region.append(region_ini)
            resources_type.append(info_page[i]['items'][j]['id']['kind'])
            id_item.append(info_page[i]['items'][j]['id'][element[1]])
            url_video.append('https://www.youtube.com/watch?v='+info_page[i]['items'][j]['id'][element[1]])
            channel.append(info_page[i]['items'][j]['snippet']['channelTitle'])
            channel_id.append(info_page[i]['items'][j]['snippet']['channelId'])
            video_title.append(info_page[i]['items'][j]['snippet']['title'])
            video_description.append(info_page[i]['items'][j]['snippet']['description'])
            created_at.append(info_page[i]['items'][j]['snippet']['publishedAt'])
    
    except :
        pass

data_req1 = {
    'batch_etag':batch_etag,
    'prev_pag_token':prev_pag_token,
    'next_pag_token':next_pag_token,
    'region':region,
    'resources_type':resources_type,
    'id_item':id_item,
    'url_video':url_video,
    'channel':channel,
    'channel_id':channel_id,
    'video_title':video_title,
    'video_description':video_description,
    'created_at':created_at  
}

df_req1 = pd.DataFrame(data_req1)

# Get  videos only
df_req1 = df_req1[df_req1['resources_type']=='youtube#video']
df_req1 = df_req1.reset_index()
df_req1 = df_req1.drop(columns= 'index')
video_ids = list(df_req1['id_item'])

# Get features
features = []

for i in tqdm(range(len(video_ids))):
    
    url_features = 'https://youtube.googleapis.com/youtube/v3/videos?part=statistics&part=contentDetails&part=topicDetails&part=recordingDetails&id='+video_ids[i]+'&key='+DEVELOPER_KEY
    response_feature = requests.get(url_features)
    
    try :
        if response_feature.status_code == 200:
            features.append(json.loads(response_feature.content))
    except :
        print('Aqui vamos ' + str(i))
        print(response_feature)
        
    time.sleep(random.choices(tiempo)[0])

df_req1['features'] = features

# Cleaning Data
id_video = []
etag_video = []
duration_video = []
views_video = []
like_video = []
dislike_video = []
favoriteCount = []
commentCount = []
topicCategories = []
recordingDetails = []

for i in tqdm(range(len(df_req1))):
    
    try : 
        id_video.append(df_req1['features'][i]['items'][0]['id']) # ID_video
    except :
        id_video.append('NO_DATA')
        
    try :    
        etag_video.append(df_req1['features'][i]['items'][0]['etag']) # etag_video
    except :
        etag_video.append('NO_DATA')
    
    
    try :
        duration_video.append(df_req1['features'][i]['items'][0]['contentDetails']['duration']) # duration_video
    except:
        duration_video.append('NO_DATA')
    
    try:
        views_video.append(df_req1['features'][i]['items'][0]['statistics']['viewCount']) # views_video
    except :
        views_video.append('NO_DATA')

    try :
        like_video.append(df_req1['features'][i]['items'][0]['statistics']['likeCount']) # like_video
    except : 
        like_video.append('NO_DATA')  
                          
    try:
        dislike_video.append(df_req1['features'][i]['items'][0]['statistics']['dislikeCount']) # dislike_video
    except :
        dislike_video.append('NO_DATA')
                             
    try:
        favoriteCount.append(df_req1['features'][i]['items'][0]['statistics']['favoriteCount']) # favourite_video
    except :
        favoriteCount.append('NO_DATA')
                             
    try :
        commentCount.append(df_req1['features'][i]['items'][0]['statistics']['commentCount']) # comment_count_video
    except :
        commentCount.append('NO_DATA')
    
    try:
        topicCategories.append(df_req1['features'][i]['items'][0]['topicDetails']['topicCategories'][0]) # Categories_video
    except :
        topicCategories.append('NO_DATA') 
    
    try:
        recordingDetails.append(df_req1['features'][i]['items'][0]['topicDetails']['recordingDetails'][0]) # recording_details
    except :
        recordingDetails.append('NO_DATA')  


# Final DataFrame
df_feature = {
    'id_video':id_video,
    'etag_video':etag_video,
    'duration_video':duration_video,
    'views_video':views_video,
    'like_video':like_video,
    'dislike_video':dislike_video,
    'favoriteCount':favoriteCount,
    'commentCount':commentCount,
    'topicCategories':topicCategories,
    'recordingDetails':recordingDetails}
df_feature_fin = pd.DataFrame(df_feature)
df_feature_fin = df_feature_fin.rename(columns={'id_video':'id_item'})
df = pd.merge(df_req1,df_feature_fin,how='inner',on='id_item')
df['duration'] = df['duration_video'].apply(lambda x : x.split('PT')[1].replace('H',' 3600+')
                                            .replace('M',' 60+').replace('S','')
                                            .split('+'))

seconds = []

for i in range(len(df['duration'])):
    temp = []
    for j in range(len(df['duration'][i])):
        temp.append(np.product(list(map(float, df['duration'][i][j].split()))))
    
    seconds.append(sum(temp))
    
df['time_video(seconds)'] = seconds
df['views_video'] = df['views_video'].apply(lambda x : int(float(x))) 
df['like_video'] = df['like_video'].apply(lambda x : int(float(x)))
df['commentCount'] = df['commentCount'].apply(lambda x : int(float(x)))

# Dataframe gold
hour = datetime.datetime.now()
date = hour.strftime("%Y-%m-%d")
df_gold = df[['video_title','video_description','url_video','created_at','views_video','like_video','dislike_video','time_video(seconds)']]
df_gold['Datefile'] = date

# Create CSV File
df_gold.to_csv('stats_videos_'+date.replace('-','')+'.csv')

# Envio de archivos a S3
s3 = boto3.resource("s3")
BUCKET = "de-raw-demo"

# Upload file to S3
s3.Bucket(BUCKET).upload_file('stats_videos_'+date.replace('-','')+'.csv','stats_videos_'+date.replace('-','')+'.csv')
os.remove('stats_videos_'+date.replace('-','')+'.csv')

print('/n')
print('FIN DEL SCRIPT')

