"""

************************************************ 
Autor = @lexbonella                            *  
Fecha = '31/01/2020'                           * 
Nombre = Web Scrapping resultados de loteria   * 
************************************************ 

"""

import pandas as pd
import requests
from bs4 import BeautifulSoup
import json
import pymongo
from pymongo import MongoClient
import os 

"""Establecemos nuestro directorio principal"""
os.chdir('/home/alexbonella2806/API_FLASK')


"""Inicializamos nuestro cliente mongodb"""
client = MongoClient('mongodb://localhost:27017/')

"""Creamos nuestra base de datos"""
mydb = client["db_loterias"] # Database name
mycoll=mydb["resultados"] # Colecction name 

"""Leemos los datos de nuestra webiste"""
df=pd.read_html('https://resultadodelaloteria.com/colombia/') # Lectura del webiste

"""Loterias que jugaron el dia anterior"""
lot_ant=df[1]
lot_ant=lot_ant.rename(columns={0:'Loteria'})
lot_ant=lot_ant.drop(index=[0],axis=1)
lot_ant=lot_ant.reset_index(inplace=False)
lot_ant=lot_ant.drop(columns='index')

"""Leemos archivo con las Url's de todas las loterias"""
df_lot=pd.read_csv('Loterias_link.csv')
df_lot=df_lot.drop(columns='Unnamed: 0')

"""Merge para rescatar los links de las loterias"""
df_final=pd.merge(df_lot,lot_ant,how='inner',on='Loteria')


"""Extraemos los resultados de todas las loterias"""
last_day=[]
loteria=[]
for i in range(len(df_final['url'])):
    a=pd.read_html(df_final['url'][i])
    temp=pd.DataFrame(a[1].loc[0])
    last_day.append(temp.T)
    loteria.append(df_final['Loteria'][i])

# Creación del Dataframe final
dia_anterior=pd.concat(last_day,ignore_index=True)
dia_anterior['Loteria']=loteria

"""Conversión a formato clave valor """
resultados=[]
for i in range(len(dia_anterior)):
    resultados.append({
        'Loteria': str(dia_anterior['Loteria'][i]),
        'Resultado':str(dia_anterior['Resultado'][i]),
        'Fecha':str(dia_anterior['Fecha'][i]),
        'Sorteo N° ':str(dia_anterior['# Sorteo'][i])
})

data_insert = mycoll.insert_many(resultados)

"""Creamos nuestro archivo csv"""
file=pd.DataFrame(resultados)
file.to_csv('result.csv')